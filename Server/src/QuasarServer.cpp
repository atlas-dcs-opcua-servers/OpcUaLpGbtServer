/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <thread>

#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>

#include <DRoot.h>
#include <DAnalogPeripherals.h>
#include <DAdcInput.h>
#include "../../Device/include/DLpGbt.h"

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
        for (auto lpgbt : Device::DRoot::getInstance()->lpgbts())
		{
			LOG(Log::TRC) << "Polling for lpGBT with address: " << lpgbt->address();
			for (auto adcPeripherals : lpgbt->analogperipheralss())
            {
                adcPeripherals->update();
            }
		}

		constexpr unsigned int SPINLOCK_WAIT_TIME_MILLISECONDS = 100; // This should be set such that it will not impact the CPU usage
		LOG(Log::TRC) << std::this_thread::get_id() << ":: spinlock for " << SPINLOCK_WAIT_TIME_MILLISECONDS << " milliseconds";
		std::this_thread::sleep_for(std::chrono::milliseconds(SPINLOCK_WAIT_TIME_MILLISECONDS));
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server.";
    for (auto lpgbt : Device::DRoot::getInstance()->lpgbts())
    	lpgbt->initialize();

}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
    for (auto lpgbt : Device::DRoot::getInstance()->lpgbts())
    	lpgbt->shutdown();
}

void QuasarServer::initializeLogIt()
{
	BaseQuasarServer::initializeLogIt();
    LOG(Log::INF) << "Logging initialized.";
}
